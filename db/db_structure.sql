SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS space_nuggets DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE space_nuggets;

CREATE USER 'sn_user'@'localhost' IDENTIFIED BY '123456789';
GRANT ALL PRIVILEGES ON * . * TO 'sn_user'@'localhost';

DROP TABLE IF EXISTS game_records;
CREATE TABLE game_records (
  `id` int(10) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `played_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` int(11) NOT NULL,
  `seconds_passed` double NOT NULL,
  `bullets_hit` int(11) NOT NULL,
  `bullets_fired` int(11) NOT NULL,
  `damage_dealt` int(11) NOT NULL,
  `damage_taken` int(11) NOT NULL,
  `bullets_closely_dodged` int(11) NOT NULL,
  `enemies_killed` int(11) NOT NULL,
  `asteroids_destroyed` int(11) NOT NULL,
  `asteroid_pieces_destroyed` int(11) NOT NULL,
  `crashed_into_asteroids` int(11) NOT NULL,
  `gold_nuggets_picked_up` int(11) NOT NULL,
  `weapon_crates_picked_up` int(11) NOT NULL,
  `health_crates_picked_up` int(11) NOT NULL,
  `total_time_in_double_damage` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
