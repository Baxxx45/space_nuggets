//////////////////////////////////////////////
///////////////////////////// GAME SYSTEM
//////////////////////////////////////////////

  class SpaceNuggetsGame {
    constructor(params) {
      this.settings = new SNSystemSettings({ game: this });
      this.api = new SNApiConnection({ game: this, api_address: params.api_address });

      // set canvas, render stuff
      let canvas = document.createElement("canvas");
      this.ctx = canvas.getContext("2d");
      canvas.height = this.settings.height;
      canvas.width = this.settings.width;
      document.getElementById("sn_game_wrapper").appendChild(canvas);
      requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || window.mozRequestAnimationFrame;

      this.ui = new SNGameGUIRendering({ game: this });
      this.dev_render = new SNDevRendering({ game: this });
      this.menu = new SNMainMenu({ game: this });

      // keyboard control handler
      this.keyboard = new SNKeyboardHandler();

      // logic handlers
      this.collision_manager = new SNCollisionManager({ game: this })
      this.state = new SNStateManager({ game: this });

      // load resources
      this.load_resources(params.root_path);

      this.background = new SNBackground({ game: this });

      // start the main loop
      this.wait_until_resources_are_loaded_and_start();
    }
    set_starting_point() {
      this.logic = new SNGameLogic({ game: this })
      this.wave_generator = new SNGameLevelGenerator({ game: this });
      this.object_list = {};
      this.player = new SNPlayer({ game: this, x: this.settings.width / 2, y: this.settings.height - 100, velocity: 0, max_velocity: 260, size: 60, image: "player", hitbox_radius: 30 });
    }
    main_loop() {
      let now = Date.now();
      let tick_duration = (now - this.then) / 1000;

      this.settings.fps = 1 / tick_duration;

      this.state.change_state_if_necessary();
      this.update(tick_duration);
      this.render_frame();

      this.then = now;
      requestAnimationFrame(() => { this.main_loop(); });
    }
    load_resources(root_path) {
      this.resources = new SNResourceManager({ game: this, root_path: root_path });
      this.resources.load_resources();
    }
    wait_until_resources_are_loaded_and_start() {
      if (this.resources.all_loaded) {
        this.then = Date.now();
        this.resources.music.music2.play();
        this.main_loop();
      } else {
        setTimeout(() => {
          this.wait_until_resources_are_loaded_and_start();
        }, 100);
      }
    }
    render_frame() {
      this.background.render();

      if (this.state.running || this.state.paused) {
        this.player.render();
        for (let id in this.object_list) {
          let obj = this.object_list[id];
          obj.render();
        }
        if (this.settings.draw_hitboxes) {
          this.dev_render.render_hitboxes();
        }
        if (this.settings.draw_fps) {
          this.dev_render.render_fps();
        }
        this.ui.render();
      }

      if (this.menu) {
        this.menu.render();
      }
    }
    update(tick_duration) {
      if (this.state.running || this.state.stopped) {
        this.background.update(tick_duration);
      }

      if (this.state.running) {
        this.player.update_position(tick_duration);
        this.player.update_effects(tick_duration);

        for (let id in this.object_list) {
          let obj = this.object_list[id];
          if (obj instanceof SNEnemyShip) {
            obj.ai.behave();
          }
          obj.update_position(tick_duration);
        }

        this.collision_manager.check_collisions();

        this.wave_generator.work();

        this.logic.add_seconds_passed(tick_duration);
      }
    }
  }

  class SNStateManager {
    constructor(params) {
      this.game = params.game;
      this.stop();
    }
    get stopped() { return this.state == "stopped"; }
    get running() { return this.state == "running"; }
    get paused() { return this.state == "paused"; }
    stop() { this.state = "stopped"; }
    pause() { this.state = "paused"; }
    run() { this.state = "running"; }

    change_state_if_necessary() {
      if (this.game.keyboard.pause(true)) {
        if (this.running) {
          this.pause();
        } else if (this.paused) {
          this.run();
        }
      }
    }
  }

  class SNCollisionManager {
    constructor(params) {
      this.game = params.game;
    }
    check_collisions() {
      for (let id in this.game.object_list) {
        let obj1 = this.game.object_list[id];

        if (obj1 instanceof SNHittableGameObject) {
          // BULLETS
          if (obj1 instanceof SNBullet) {
            // PLAYERS BULLET
            if (obj1.weapon.owner == this.game.player) {
              for (let obj2_id in this.game.object_list) {
                let obj2 = this.game.object_list[obj2_id];
                // vs enemy ship
                if (obj2 instanceof SNSpaceShip) {
                  if (obj1.collides_with(obj2)) {
                    this.game.logic.enemy_ship_hit_by_player(obj2, obj1);
                  }
                }
                // vs asteroid
                if (obj2 instanceof SNAsteroid) {
                  if (obj1.collides_with(obj2)) {
                    this.game.logic.asteroid_hit_by_player(obj2, obj1);
                  }
                }
                // vs asteroid piece
                if (obj2 instanceof SNAsteroidPiece) {
                  if (obj1.collides_with(obj2)) {
                    this.game.logic.asteroid_piece_hit_by_player(obj2, obj1);
                  }
                }
              }
            }

            // ENEMY BULLET
            if (obj1.weapon.owner instanceof SNEnemyShip) {
              if (obj1.collides_with(this.game.player)) {
                this.game.logic.player_hit_by_enemy_bullet(obj1);
              } else if (obj1.real_distance_to_hittable(this.game.player) <= 10) {
                this.game.player.stats.add_bullet_closely_dodged(obj1);
              }
            }
          }

          // ASTEROIDS
          if (obj1 instanceof SNAsteroid) {
            if (obj1.collides_with(this.game.player)) {
              this.game.logic.player_hit_by_asteroid(obj1);
            }
          }

          // ASTEROID PIECES
          if (obj1 instanceof SNAsteroidPiece) {
            if (obj1.collides_with(this.game.player)) {
              this.game.logic.player_hit_by_asteroid_piece(obj1);
            }
          }

          // PICKUPS
          if (obj1 instanceof SNPickup) {
            // gold nugget
            if (obj1 instanceof SNGoldNugget) {
              if (obj1.collides_with(this.game.player)) {
                this.game.logic.player_picks_up_gold_nugget(obj1);
              }
            }

            if (obj1 instanceof SNPickupHealth) {
              if (obj1.collides_with(this.game.player)) {
                this.game.logic.player_picks_up_health_pack(obj1);
              }
            }

            if (obj1 instanceof SNPickupDoubleDamage) {
              if (obj1.collides_with(this.game.player)) {
                this.game.logic.player_picks_up_double_damage(obj1);
              }
            }

            if (obj1 instanceof SNPickupWeapon) {
              if (obj1.collides_with(this.game.player)) {
                this.game.logic.player_picks_up_weapon_crate(obj1);
              }
            }
          }
        }
      }
    }
  }

  class SNGameLogic {
    constructor(params) {
      this.game = params.game;
      this.seconds_passed = 0;
    }
    enemy_ship_hit_by_player(ship, bullet) {
      bullet.hit(ship);
      if (ship.destroyed) {
        this.enemy_ship_destroyed_by_player(ship);
      }
    }
    enemy_ship_destroyed_by_player(ship) {
      this.game.player.stats.enemies_killed++;
      this.game.player.stats.add_score(ship.score_worth);
      new SNGameUIFlashingText({ game: this.game, text: "+" + ship.score_worth, color: "white", x: ship.x, y: ship.y, duration: 1 });
    }
    asteroid_hit_by_player(asteroid, bullet) {
      bullet.hit(asteroid);
      if (asteroid.destroyed) {
        this.asteroid_destroyed_by_player(asteroid);
      }
    }
    asteroid_destroyed_by_player(asteroid) {
      this.game.player.stats.asteroids_destroyed++;
      this.game.player.stats.add_score(asteroid.score_worth);
      new SNGameUIFlashingText({ game: this.game, text: "+" + asteroid.score_worth, color: "white", x: asteroid.x, y: asteroid.y, duration: 1 });
    }
    asteroid_piece_hit_by_player(asteroid_piece, bullet) {
      bullet.hit(asteroid_piece);
      if (asteroid_piece.destroyed) {
        this.asteroid_piece_destroyed_by_player(asteroid_piece);
      }
    }
    asteroid_piece_destroyed_by_player(asteroid_piece) {
      this.game.player.stats.asteroid_pieces_destroyed++;
    }
    player_hit_by_enemy_bullet(bullet) {
      bullet.hit(this.game.player);
    }
    player_hit_by_asteroid(asteroid) {
      asteroid.kill();
      this.game.player.damage(2);
      this.game.player.stats.crashed_into_asteroids++;
    }
    player_hit_by_asteroid_piece(asteroid_piece) {
      asteroid_piece.kill();
      this.game.player.damage(1);
      this.game.player.stats.crashed_into_asteroids++;
    }
    player_picks_up_gold_nugget(gold_nugget) {
      gold_nugget.destroy_object();
      this.game.player.stats.add_score(1000);
      new SNGameUIFlashingText({ game: this.game, text: "+1000", color: "yellow", x: gold_nugget.x, y: gold_nugget.y, duration: 1 });
      this.game.resources.sounds.pickup3.play(50);
      this.game.player.stats.gold_nuggets_picked_up++;
    }
    player_picks_up_health_pack(pickup_health) {
      pickup_health.destroy_object();
      this.game.player.health += 5;
      new SNGameUIFlashingText({ game: this.game, text: "+5 HP", color: "#ff8d8d", x: pickup_health.x, y: pickup_health.y, duration: 1 });
      this.game.resources.sounds.pickup2.play();
      this.game.player.stats.health_crates_picked_up++;
      this.game.player.stats.add_score(pickup_health.score_worth);
      new SNGameUIFlashingText({ game: this.game, text: "+" + pickup_health.score_worth, color: "#ff8d8d", x: pickup_health.x, y: pickup_health.y + 15, duration: 1 });
    }
    player_picks_up_double_damage(pickup) {
      pickup.destroy_object();
      new SNGameUIFlashingText({ game: this.game, text: "DAMAAAGE", color: "#c2ecff", size: 20, x: pickup.x, y: pickup.y, duration: 1 });
      let player = this.game.player;
      player.has_double_damage = true;
      player.double_damage_duration = 7;
      player.last_double_damage = this.seconds_passed;
      this.game.resources.sounds.double_damage_7sec.play();
      this.game.resources.sounds.pickup2.play();
      this.game.player.stats.double_damage_picked_up++;
      this.game.player.stats.add_score(pickup.score_worth);
      new SNGameUIFlashingText({ game: this.game, text: "+" + pickup.score_worth, color: "#c2ecff", x: pickup.x, y: pickup.y + 15, duration: 1 });
    }
    player_picks_up_weapon_crate(pickup) {
      pickup.destroy_object();
      let player = this.game.player;
      let weapon = new pickup.weapon_class({ owner: player });
      weapon.pickup_sound.play(weapon.pickup_sound_volume);
      if (player.weapon instanceof pickup.weapon_class) {
        if (!player.weapon.is_max_level) {
          player.weapon.level++;
          new SNGameUIFlashingText({ game: this.game, text: weapon.messages.on_levelup, color: weapon.messages.color, size: 20, x: pickup.x, y: pickup.y, duration: 1 });
        } else {
          new SNGameUIFlashingText({ game: this.game, text: weapon.messages.on_already_max_level, color: weapon.messages.color, size: 20, x: pickup.x, y: pickup.y, duration: 1 });
        }
      } else {
        let new_weapon_level = !player.weapon.is_min_level ? player.weapon.level - 1 : 1;
        weapon.level = new_weapon_level;
        player.weapon = weapon;
        new SNGameUIFlashingText({ game: this.game, text: weapon.messages.on_pickup, color: weapon.messages.color, size: 20, x: pickup.x, y: pickup.y, duration: 1 });
      }
      this.game.player.stats.weapon_crates_picked_up++;
      this.game.player.stats.add_score(pickup.score_worth);
      new SNGameUIFlashingText({ game: this.game, text: "+" + pickup.score_worth, color: weapon.messages.color, x: pickup.x, y: pickup.y + 15, duration: 1 });
    }
    player_dies() {
      this.game.player.y = 3000;
      this.game.menu = new SNEndOfGameMenu({ game: this.game, stats: this.game.player.stats });
      this.game.api.send_game_record();
    }

    add_seconds_passed(tick) {
      this.seconds_passed += tick;
    }
  }

  class SNKeyboardHandler {
    constructor() {
      this.keys_down = {};
      this.reading_input = false;
      this.input_buffer = [];
      addEventListener("keydown", (e) => {
        this.keys_down[e.keyCode] = true;
        if (this.reading_input) {
          this.input_buffer.push(e.keyCode);
        }
      }, false);
      addEventListener("keyup", (e) => {
        delete this.keys_down[e.keyCode];
      }, false);
    }

    check_code(code, unclick_button) {
      let button_clicked = code in this.keys_down;
      if (unclick_button) {
        delete this.keys_down[code];
      }
      return button_clicked;
    }

    move_up(unclick = false)    { return this.check_code(38, unclick); } // up
    move_down(unclick = false)  { return this.check_code(40, unclick); } // down
    move_left(unclick = false)  { return this.check_code(37, unclick); } // left
    move_right(unclick = false) { return this.check_code(39, unclick); } // right
    fire(unclick = false)       { return this.check_code(32, unclick); } // space
    pause(unclick = false)      { return this.check_code(80, unclick); } // 'p'
    enter(unclick = false)      { return this.check_code(13, unclick); } // enter

    start_input() { this.reading_input = true; this.input_buffer = []; }
    stop_input() { this.reading_input = false; }
    load_input() {
      let string = "";
      let buffer_copy = this.input_buffer.slice(0);
      let code = false;
      while (code = buffer_copy.shift()) {
        if ((code >= 48 && code <= 57) || // is a number
            (code >= 65 && code <= 90) || // is a letter
             code == 32)
        { // is a space
          string += String.fromCharCode(code);
        } else if (code == 8) { // is backspace
          string = string.substring(0, string.length -1);
        }
      }
      return string;
    }
  }

  class SNSystemSettings {
    constructor(params) {
      this.game = params.game;
      this.width = 500;
      this.height = 800;
      this.draw_fps = false;
      this.fps = 0;
      this.draw_background_stars = true;
      this.draw_hitboxes = false;
    }
  }

  class SNGameGUIRendering {
    constructor(params) {
      this.game = params.game;
      this.flashing_texts = {};
    }
    render() {
      this.render_total_score();
      this.render_player_health();
      this.render_flashing_texts();
    }
    render_total_score() {
      let score = this.game.player.stats.score;
      let ctx = this.game.ctx;
      ctx.font = "30px Terran";
      ctx.fillStyle = "white";
      ctx.textAlign = "right";
      ctx.textBaseline = "bottom";
      ctx.fillText(score, this.game.settings.width - 5, this.game.settings.height - 5);
    }
    render_player_health() {
      let hp = this.game.player.health;
      let ctx = this.game.ctx;
      let image = this.game.resources.images.player_life;
      let size = 30;
      let y = this.game.settings.height - 5 - size/2;
      let x = 5 + size/2;

      image.render({ x: x, y: y, size_x: size, size_y: size });
      x += size;
      ctx.font = "30px Terran";
      ctx.fillStyle = "white";
      ctx.textAlign = "left";
      ctx.textBaseline = "middle"
      ctx.fillText(hp, x - 10, y);
    }
    render_flashing_texts() {
      for (let id in this.flashing_texts) {
        let flashing_text = this.flashing_texts[id];
        flashing_text.render();
      }
    }
  }

  class SNGameUIFlashingText {
    constructor(params) {
      this.game = params.game;
      this.text = params.text;
      this.x = params.x;
      this.y = params.y;
      this.size = params.size || 17;
      this.color = params.color || "white";
      this.weight = params.weight || "";
      this.duration = params.duration || 1;
      this.flashed_at = Date.now();
      this.id = this.flashed_at;
      while(this.game.ui.flashing_texts[this.id]) this.id++;
      this.game.ui.flashing_texts[this.id] = this;
    }
    render() {
      let ctx = this.game.ctx;
      ctx.font = this.size + "px Terran " + this.weight;
      ctx.fillStyle = this.color;
      ctx.textAlign = "left";
      ctx.textBaseline = "bottom";

      let text_width = ctx.measureText(this.text).width;
      if (this.x + text_width >= this.game.settings.width - 5) {
        this.x = this.game.settings.width - 5 - text_width; // so it doesnt get out of screen
      }
      ctx.fillText(this.text, this.x, this.y);
      this.y -= 0.5;
      if (Date.now() - this.flashed_at > this.duration * 1000) {
        this.destroy_object();
      }
    }
    destroy_object() {
      delete this.game.ui.flashing_texts[this.id];
    }
  }

  class SNDevRendering {
    constructor(params) {
      this.game = params.game;
    }
    render_fps() {
      let ctx = this.game.ctx;
      ctx.fillStyle = "rgb(250, 250, 250)";
      ctx.font = "24px Helvetica";
      ctx.textAlign = "left";
      ctx.textBaseline = "bottom";
      ctx.fillText("FPS: " + Math.round(this.game.settings.fps), 32, 32);
    }
    draw_circle(params) {
      let ctx = this.game.ctx;
      ctx.beginPath();
      ctx.arc(params.x, params.y, params.size, 0, 2 * Math.PI, false);
      ctx.fillStyle = "rgba(" + params.rgba[0] + ", " + params.rgba[1] + ", " + params.rgba[2] + ", " + params.rgba[3] + ")";
      ctx.fill();
    }
    render_hitboxes() {
      let player = this.game.player;
      this.draw_circle({ x: player.x, y: player.y, size: player.hitbox_radius, rgba: [255, 100, 0, 0.3]});
      this.draw_circle({ x: player.x, y: player.y, size: 1, rgba: [255, 0, 0, 1]});

      for (let id in this.game.object_list) {
        let obj = this.game.object_list[id];
        if (obj instanceof SNHittableGameObject) {
          this.draw_circle({ x: obj.x, y: obj.y, size: obj.hitbox_radius, rgba: [255, 100, 0, 0.3]});
          this.draw_circle({ x: obj.x, y: obj.y, size: 1, rgba: [255, 0, 0, 1]});
        }
      }
    }
  }

  class SNMenuButton {
    constructor(params) {
      this.menu = params.menu;
      this.text = params.text;
      this.callback = params.callback;
      this.selected = params.selected || false;
      this.rel_x = params.rel_x;
      this.rel_y = params.rel_y;
      this.width = params.width;
      this.height = params.height || 70;
    }
    select() { this.selected = true; }
    unselect() { this.selected = false; }
    press() {
      this.callback(this.menu.game);
    }
    render() {
      let x = this.menu.x + this.rel_x;
      let y = this.menu.y + this.rel_y;
      let ctx = this.menu.game.ctx;

      let background_color = this.selected ? "rgb(122, 122, 170)" : "rgb(122, 122, 122)";
      let text_color = this.selected ? "white" : "rgb(200, 200, 200)";

      ctx.fillStyle = background_color;
      ctx.globalAlpha = 0.7;
      ctx.fillRect(x, y, this.width, this.height);
      ctx.globalAlpha = 1.0;

      ctx.font = "30px Terran";
      ctx.fillStyle = text_color;
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      ctx.fillText(this.text, x + this.width/2, y + this.height/2);
    }
  }

  class SNMenuPanel {
    constructor(params) {
      this.menu = params.menu;
      this.text = params.text;
      this.callback = params.callback;
      this.selected = params.selected || false;
      this.rel_x = params.rel_x;
      this.rel_y = params.rel_y;
      this.width = params.width;
      this.height = params.height || 70;
      this.text_color = params.text_color || "white"
      this.font_size = params.font_size || 30;
    }
    render() {
      let x = this.menu.x + this.rel_x;
      let y = this.menu.y + this.rel_y;
      let ctx = this.menu.game.ctx;

      let background_color = "rgb(122, 122, 122)";
      let text_color = this.text_color;

      ctx.fillStyle = background_color;
      ctx.globalAlpha = 0.1;
      ctx.fillRect(x, y, this.width, this.height);
      ctx.globalAlpha = 1.0;

      ctx.font = this.font_size + "px Terran";
      ctx.fillStyle = text_color;
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      ctx.fillText(this.text, x + this.width/2, y + this.height/2);
    }
  }

  class SNMenuImagePanel {
    constructor(params) {
      this.menu = params.menu;
      this.rel_x = params.rel_x || 0;
      this.rel_y = params.rel_y || 0;
      this.width = params.width;
      this.height = params.height;
      this.image = params.image;
    }
    render() {
      let x = this.menu.x + this.rel_x;
      let y = this.menu.y + this.rel_y;

      this.menu.game.resources.images[this.image].render({ size_x: this.width, size_y: this.height, x: x, y: y });
    }
  }

  class SNHighScorePanel extends SNMenuPanel {
    constructor(params) {
      super(params);
      this.height = params.height || 30;
      this.rank = params.rank;
      this.name = "";
      this.score = "";
    }
    render() {
      let x = this.menu.x + this.rel_x;
      let y = this.menu.y + this.rel_y;
      let ctx = this.menu.game.ctx;

      let background_color = "rgb(122, 122, 122)";
      let text_color = "white";

      ctx.fillStyle = background_color;
      ctx.globalAlpha = 0.1;
      ctx.fillRect(x, y, this.width, this.height);
      ctx.globalAlpha = 1.0;

      ctx.font = "20px Terran";
      ctx.fillStyle = text_color;
      ctx.textBaseline = "middle";
      ctx.textAlign = "left";
      ctx.fillText(this.rank + ". ", x + 10, y + this.height/2);
      ctx.fillText(this.name, x + 40, y + this.height/2);
      ctx.textAlign = "right";
      ctx.fillText(this.score, x + this.width - 10, y + this.height/2);
    }
  }

  class SNMenu {
    constructor(params) {
      this.game = params.game;
      this.x = 50;
      this.y = 200;
      this.width = this.game.settings.width - 100;
      this.height = 290;
      this.selected_button = 0;
      this.buttons = [];
      this.panels = [];
      this.images = [];
      this.background_color = "rgb(122, 122, 122)";
    }
    render() {
      let ctx = this.game.ctx;
      ctx.fillStyle = this.background_color;
      ctx.globalAlpha = 0.5;
      ctx.fillRect(this.x, this.y, this.width, this.height);
      ctx.globalAlpha = 1.0;
      this.buttons.forEach((button) => {
        button.render();
      });
      this.panels.forEach((panel) => {
        panel.render();
      });
      this.images.forEach((image) => {
        image.render();
      });
      this.button_control();
    }
    button_control() {
      let keyboard = this.game.keyboard;
      if (keyboard.move_up() && this.selected_button > 0) {
        this.buttons[this.selected_button].unselect();
        this.selected_button--;
        this.buttons[this.selected_button].select();
      }
      if (keyboard.move_down() && this.selected_button < this.buttons.length - 1) {
        this.buttons[this.selected_button].unselect();
        this.selected_button++;
        this.buttons[this.selected_button].select();
      }

      if (keyboard.enter(true)) {
        this.buttons[this.selected_button].press();
      }
    }
  }

  class SNMainMenu extends SNMenu {
    constructor(params) {
      super(params);
      this.y = 350;
      this.height = 200;
      this.images = [
        new SNMenuImagePanel({ menu: this, image: "game_logo", rel_x: this.width / 2, width: 500, height: 328 })
      ]
      this.buttons = [
        new SNMenuButton({ menu: this, text: "New Game", callback: this.new_game, selected: true, rel_x: 20, rel_y: 20, width: this.width - 40 }),
        new SNMenuButton({ menu: this, text: "High Scores", callback: this.high_scores, rel_x: 20, rel_y: 110, width: this.width - 40 })
      ]
    }
    new_game(game) {
      game.menu = new SNStartingMenu({ game: game });
    }
    high_scores(game) {
      game.menu = new SNHighScoresMenu({ game: game });
    }
  }

  class SNStartingMenu extends SNMenu {
    constructor(params) {
      super(params);
      this.panels = [
        new SNMenuPanel({ menu: this, text: "Enter name", rel_x: 20, rel_y: 20, width: this.width - 40 }),
        new SNMenuPanel({ menu: this, text: "", rel_x: 20, rel_y: 110, width: this.width - 40 })
      ]
      this.buttons = [
        new SNMenuButton({ menu: this, text: "Start", selected: true, callback: this.start, rel_x: 20, rel_y: 200, width: this.width - 40 })
      ]
      this.game.keyboard.start_input();
    }
    render() {
      let text = this.game.keyboard.load_input();
      if (!text) { text = Math.floor(Date.now() / 500) % 2 == 0 ? "/" : ""; } // cursor

      this.panels[1].text = text;
      super.render();
    }
    start(game) {
      let player_name = game.keyboard.load_input();
      game.player_name = player_name;
      game.keyboard.stop_input();
      game.set_starting_point();
      game.state.run();
      game.menu = false;
    }
  }

  class SNHighScoresMenu extends SNMenu {
    constructor(params) {
      super(params);
      this.panels = [
        new SNMenuPanel({ menu: this, text: "High Scores", rel_x: 20, rel_y: 20, width: this.width - 40 }),
        new SNHighScorePanel({ menu: this, rank: 1, rel_x: 20, rel_y: 110, width: this.width - 40, height: 30 }),
        new SNHighScorePanel({ menu: this, rank: 2, rel_x: 20, rel_y: 150, width: this.width - 40, height: 30 }),
        new SNHighScorePanel({ menu: this, rank: 3, rel_x: 20, rel_y: 190, width: this.width - 40, height: 30 }),
        new SNHighScorePanel({ menu: this, rank: 4, rel_x: 20, rel_y: 230, width: this.width - 40, height: 30 }),
        new SNHighScorePanel({ menu: this, rank: 5, rel_x: 20, rel_y: 270, width: this.width - 40, height: 30 })
      ]
      this.buttons = [
        new SNMenuButton({ menu: this, text: "Back", selected: true, callback: this.back_to_main_menu, rel_x: 20, rel_y: 320, width: this.width - 40 })
      ]
      this.height = 410;
      this.get_high_scores();
    }
    get_high_scores() {
      this.game.api.get_high_scores();
    }
    back_to_main_menu(game) {
      game.menu = new SNMainMenu({ game: game });
    }
    display_high_scores(high_scores) {
      for (let i=0; i<high_scores.length; i++) {
        this.panels[i+1].name = high_scores[i].name;
        this.panels[i+1].score = high_scores[i].score;
      }
    }
  }

  class SNEndOfGameMenu extends SNMenu {
    constructor(params) {
      super(params);
      this.y = 20;
      this.height = this.game.settings.height - 40;
      this.stats = params.stats;
      this.panels = [
        new SNMenuPanel({ menu: this, text: "Game Over", rel_x: 20, rel_y: 20, width: this.width - 40 }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 0 * 40 + 0 * 30, width: this.width - 40, height: 40, font_size: 25, text: "Score: " + this.stats.score }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 0 * 30 + 1 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Accuracy: " + (Math.round(this.stats.bullets_hit * 100 * 100 / this.stats.bullets_fired) / 100) + "% (" + this.stats.bullets_hit  + "/" + this.stats.bullets_fired + ")" }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 1 * 30 + 2 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Damage dealt: " + this.stats.damage_dealt }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 2 * 30 + 3 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Damage taken: " + this.stats.damage_taken }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 3 * 30 + 4 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Bullets closely dodged: " + this.stats.bullets_closely_dodged.length }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 4 * 30 + 5 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Enemies killed: " + this.stats.enemies_killed }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 5 * 30 + 6 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Asteroids destroyed: " + this.stats.asteroids_destroyed }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 6 * 30 + 7 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Asteroid pieces: " + this.stats.asteroid_pieces_destroyed }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 7 * 30 + 8 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Asteroid crashes: " + this.stats.crashed_into_asteroids }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 8 * 30 + 9 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Gold Nuggets: " + this.stats.gold_nuggets_picked_up, text_color: "yellow" }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 9 * 30 + 10 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Weapon crates: " + this.stats.weapon_crates_picked_up }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 10 * 30 + 11 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Health crates: " + this.stats.health_crates_picked_up }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 11 * 30 + 12 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Time in game: " + Math.round(this.stats.seconds_passed * 100) / 100 + " sec" }),
        new SNMenuPanel({ menu: this, rel_x: 20, rel_y: 2 * 20 + 70 + 1 * 40 + 12 * 30 + 13 * 9, width: this.width - 40, height: 30, font_size: 20, text: "Damage mode: " + Math.round(this.stats.total_time_in_double_damage * 100) / 100 + " sec" }),
      ]
      this.buttons = [
        new SNMenuButton({ menu: this, text: "Main Menu", selected: true, callback: this.back_to_main_menu, rel_x: 20, rel_y: this.height - 90, width: this.width - 40 })
      ]
    }
    back_to_main_menu(game) {
      game.menu = new SNMainMenu({ game: game });
    }
  }

  class SNApiConnection {
    constructor(params) {
      this.game = params.game;
      this.api_address = params.api_address || "";
    }
    get_high_scores() {
      let url_params = { action: "get_high_scores" }
      this.send_request(url_params, this.display_high_scores);
    }
    send_game_record() {
      let stats = this.game.player.stats;
      let url_params = {
        action:                      "add_play_record",
        name:                        this.game.player_name || "Unnamed",
        score:                       stats.score,
        bullets_hit:                 stats.bullets_hit,
        bullets_fired:               stats.bullets_fired,
        damage_dealt:                stats.damage_dealt,
        damage_taken:                stats.damage_taken,
        bullets_closely_dodged:      stats.bullets_closely_dodged.length,
        enemies_killed:              stats.enemies_killed,
        asteroids_destroyed:         stats.asteroids_destroyed,
        asteroid_pieces_destroyed:   stats.asteroid_pieces_destroyed,
        crashed_into_asteroids:      stats.crashed_into_asteroids,
        gold_nuggets_picked_up:      stats.gold_nuggets_picked_up,
        weapon_crates_picked_up:     stats.weapon_crates_picked_up,
        health_crates_picked_up:     stats.health_crates_picked_up,
        seconds_passed:              stats.seconds_passed,
        total_time_in_double_damage: stats.total_time_in_double_damage
      }
      this.send_request(url_params);
    }

    display_high_scores(game, data) {
      if (game.menu instanceof SNHighScoresMenu) {
        game.menu.display_high_scores(data.high_scores);
      }
    }

    send_request(url_params, callback) {
      let game = this.game;
      let xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = () => {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200 && typeof callback == "function") {
          callback(game, JSON.parse(xmlhttp.responseText));
        }
      };
      xmlhttp.open("GET", this.build_url(url_params), true);
      xmlhttp.send();
    }
    build_url(url_params) {
      let get_params = [];
      for(let key in url_params) {
        get_params.push(key + "=" + encodeURI(url_params[key]));
      }
      get_params = get_params.join("&");
      let url = this.api_address + "?" + get_params;
      return url;
    }
  }

///////////////////////////////////////////
/////////////////////////////// GAME LOGIC
///////////////////////////////////////////

  class SNGameLevelGenerator {
    constructor(params) {
      this.game = params.game;
      this.weapon_spawner = new SNPickupWeaponGenerator({ game: this.game });

      this.set_starting_spawning_times();
    }
    set_starting_spawning_times() {
      this.last_spawned_at = {
        enemy: 1,
        asteroid: 3,
        pickup: 5
      };
    }

    get game_time() { return this.game.logic.seconds_passed; }


    get pickup_ready_to_spawn() { return this.game_time >= this.last_spawned_at.pickup + this.pickup_cooldown; }
    get asteroid_ready_to_spawn() { return this.game_time >= this.last_spawned_at.asteroid + this.asteroid_cooldown; }
    get enemy_ready_to_spawn() { return this.game_time >= this.last_spawned_at.enemy + this.enemy_cooldown; }

    get pickup_cooldown() { return 10 / Math.log(this.game_time + 2) + 1; }
    get asteroid_cooldown() { return 8 / Math.log(this.game_time + 2); }
    get enemy_cooldown() { return 7 / Math.log(this.game_time + 2); }

    work() {
      if (this.enemy_ready_to_spawn) {
        this.generate_enemy();
      }

      if (this.asteroid_ready_to_spawn) {
        this.generate_asteroid();
      }

      if (this.pickup_ready_to_spawn) {
        this.generate_pickup();
      }
    }

    get pickup_weapon_chance_range() { return 0.50; } // 50%
    get pickup_health_chance_range() { return 0.90; } // 40%
    get pickup_double_damage_chance_range() { return 1.00; } // 10%

    get min_asteroid_pieces() { return 2; }
    get max_asteroid_pieces() { return Math.floor(this.game_time / 10) + 1 + this.min_asteroid_pieces; }
    get asteroid_breaks_into() { return Math.floor(Math.random() * (this.max_asteroid_pieces - this.min_asteroid_pieces) + this.min_asteroid_pieces); }
    get asteroid_velocity() { return Math.floor(Math.random() * 150 + 50); }
    asteroid_health(breaks_into) { return Math.floor(breaks_into * 1.5); }
    asteroid_size(breaks_into) { return breaks_into * 15 + Math.floor(Math.random() * 20 + 10); }

    get enemy_min_size() { return 40; }
    get enemy_max_size() { return 110; }
    get enemy_size() { return Math.random() * (this.enemy_max_size - this.enemy_min_size) + this.enemy_min_size; }
    get enemy_velocity() { return Math.random() * 150 + 50; }
    get enemy_health() { return Math.floor(Math.random() * 5 + 1); }
    get enemy_weapon_level() {
      if      (this.game_time < 15)  { return Math.round(Math.random() * 0 + 1); }
      else if (this.game_time < 30)  { return Math.round(Math.random() * 1 + 1); }
      else if (this.game_time < 60)  { return Math.round(Math.random() * 2 + 1); }
      else if (this.game_time < 60)  { return Math.round(Math.random() * 2 + 1); }
      else if (this.game_time < 120) { return Math.round(Math.random() * 3 + 1); }
      else if (this.game_time < 300) { return Math.round(Math.random() * 2 + 2); }
      else                           { return Math.round(Math.random() * 3 + 2); }
    }
    get enemy_ai_id() { return Math.floor(Math.random() * 4 + 1); }
    get enemy_weapon_class_name() { return ["GatlingMk1", "GatlingMk2", "PlasmaGun"][Math.floor(Math.random() * 3)]; }
    get enemy_ship_image() {
      let ship_color = ["Black", "Blue", "Green", "Red"][Math.floor(Math.random() * 4)];
      let ship_number = Math.round(Math.random() * 4 + 1);
      return "ship" + ship_color + ship_number;
    }

    generate_pickup() {
      let pickup;
      let random_number = Math.random();
      let score_worth = 0;

      if (random_number <= this.pickup_weapon_chance_range) { // weapon
        pickup = this.weapon_spawner.spawn_random();
        if (this.game.player.weapon instanceof pickup.weapon_class) {
          score_worth = 300;
        } else {
          score_worth = 1000;
        }
      } else if (random_number <= this.pickup_health_chance_range) { // health pack
        pickup = new SNPickupHealth({ game: this.game });
        score_worth = 500;
      } else if (random_number <= this.pickup_double_damage_chance_range) { // double damage
        pickup = new SNPickupDoubleDamage({ game: this.game });
        score_worth = 400;
      }

      pickup.score_worth = score_worth;
      this.last_spawned_at.pickup = this.game_time;
    }

    generate_asteroid() {
      let asteroid;

      let breaks_into = this.asteroid_breaks_into;
      let health = this.asteroid_health(breaks_into);
      let size = this.asteroid_size(breaks_into);
      let velocity = this.asteroid_velocity;

      asteroid = new SNAsteroid({ game: this.game, velocity: velocity, size: size, health: health, breaks_into: breaks_into });

      let score_worth = Math.round(breaks_into * 100 + velocity - 50);
      asteroid.score_worth = score_worth;

      this.last_spawned_at.asteroid = this.game_time;
    }

    generate_enemy() {
      let max_velocity = this.enemy_velocity;
      let health = this.enemy_health;
      let ai = this.enemy_ai_id;
      let size = this.enemy_size;
      let weapon_class_name = this.enemy_weapon_class_name;
      let weapon_level = this.enemy_weapon_level;
      let image = this.enemy_ship_image;

      let enemy = new SNEnemyShip({ game: this.game, max_velocity: max_velocity, size: size, image: image, health: health });
      let weapon = new this.weapon_spawner.list[weapon_class_name].weapon_class({ owner: enemy });
      enemy.weapon = weapon;
      weapon.level = weapon_level;

      enemy.change_ai(["standard", "aiming", "sniper", "chasing"][ai - 1]);

      let score_worth = Math.round(health * max_velocity * ((ai + 1) / 4) * (weapon_level / 2) / size * 100);
      enemy.score_worth = score_worth;

      this.last_spawned_at.enemy = this.game_time;
    }
  }

  class SNPickupWeaponGenerator {
    constructor(params) {
      this.game = params.game;
      this.list = {
        GatlingMk1: { image: "crate1", weapon_class: SNWeaponGatlingMk1 },
        GatlingMk2: { image: "crate2", weapon_class: SNWeaponGatlingMk2 },
        PlasmaGun: { image: "crate3", weapon_class: SNWeaponPlasmaGun }
      }
    }
    get name_list() {
      let names = [];
      for(let name in this.list) {
        names.push(name);
      }
      return names;
    }
    spawn_random() {
      let random_id = Math.floor(Math.random()*(this.name_list.length));
      let random_name = this.name_list[random_id];
      return new SNPickupWeapon({ game: this.game, image: this.list[random_name].image, weapon_class: this.list[random_name].weapon_class })
    }
  }

  class SNPlayerStats {
    constructor(params) {
      this.owner = params.owner;
      this.score = 0;
      this.enemies_killed = 0;
      this.asteroids_destroyed = 0;
      this.asteroid_pieces_destroyed = 0;
      this.gold_nuggets_picked_up = 0;
      this.health_crates_picked_up = 0;
      this.weapon_crates_picked_up = 0;
      this.double_damage_picked_up = 0;
      this.total_time_in_double_damage = 0;
      this.bullets_fired = 0;
      this.bullets_hit = 0;
      this.bullets_taken = 0;
      this.crashed_into_asteroids = 0;
      this.bullets_closely_dodged = [];
      this.damage_dealt = 0;
      this.damage_taken = 0;
      this.seconds_passed = 0;
    }
    add_score(score) {
      this.score += score;
    }
    add_bullet_closely_dodged(bullet) {
      if (this.bullets_closely_dodged.indexOf(bullet.id) == -1) {
        this.bullets_closely_dodged.push(bullet.id);
      }
    }
    remove_bullet_closely_dodged(bullet) {
      let index = this.bullets_closely_dodged.indexOf(bullet.id);

      if (index !== -1) {
        this.bullets_closely_dodged.splice(index, 1);
      }
    }
  }






///////////////////////////////////////////
///////////////////////////// GAME OBJECTS
///////////////////////////////////////////

  class SNGameObject {
    constructor(params) {
      this.game = params.game;

      this.x = params.x;
      this.y = params.y;

      this.id = Date.now();
      while(this.game.object_list[this.id]) this.id++;
      this.game.object_list[this.id] = this;
    }
    render() {

    }
    update_position(tick) {

    }
    destroy_object() {
      delete this.game.object_list[this.id];
    }
    get destroyed() {
      return !this.game.object_list[this.id];
    }

    distance_to(object) {
      return Math.sqrt((this.x - object.x) * (this.x - object.x) + (this.y - object.y) * (this.y - object.y));
    }
    angle_to(object) {
      let distance = this.distance_to(object);
      if (distance == 0) { return 0; }
      let diff_x = object.x - this.x;
      let diff_y = object.y - this.y;
      let angle = Math.asin(diff_x / distance);
      angle = angle * 360 / (Math.PI * 2);
      if (diff_y < 0) {
        angle = 180 - angle;
      }
      return angle;
    }
  }

  class SNMovableGameObject extends SNGameObject {
    constructor(params) {
      super(params);
      this.velocity = params.velocity;
      this.direction = params.direction;
      this.acceleration = { accelerating: false, end_velocity: 0, step: 100 }
    }
    update_position(tick) {
      this.update_velocity(tick);
      let speedX = Math.sin(this.direction * (2*Math.PI) / 360) * this.velocity;
      let speedY = Math.cos(this.direction * (2*Math.PI) / 360) * this.velocity;
      this.x += speedX * tick;
      this.y += speedY * tick;
      if (this.out_of_bounds) {
        this.destroy_object();
      }
    }
    get out_of_bounds() {
      let size = this.size || 0;
      let safety = 5;
      return  this.x + this.size/2 + safety <= 0 ||
              this.x - this.size/2 - safety >= this.game.settings.width ||
              this.y + this.size/2 + safety <= 0 ||
              this.y - this.size/2 - safety >= this.game.settings.height;
    }
    set_velocity(new_velocity) {
      if (!this.velocity) this.velocity = 0;
      if (this.velocity == new_velocity) {
        this.acceleration.accelerating = false;
      } else {
        this.acceleration.accelerating = true;
        this.acceleration.end_velocity = new_velocity;
      }
    }
    update_velocity(tick) {
      if (this.acceleration.accelerating) {
        let diff = this.acceleration.step * tick;
        if (this.velocity < this.acceleration.end_velocity) {
          if (this.velocity + diff >= this.acceleration.end_velocity) {
            this.velocity = this.acceleration.end_velocity;
            this.acceleration.accelerating = false;
          } else {
            this.velocity += diff;
          }
        } else {
          if (this.velocity - diff <= this.acceleration.end_velocity) {
            this.velocity = this.acceleration.end_velocity;
            this.acceleration.accelerating = false;
          } else {
            this.velocity -= diff;
          }
        }
      }
    }
  }

  class SNSpriteObject extends SNMovableGameObject {
    constructor(params) {
      super(params);
      this.sprite_id = this.game.resources.images[params.sprite_name].new_instance().id;
      this.size = params.size;
      this.angle = params.angle ? params.angle : Math.PI * 2 * Math.random();
    }
    render() {
      let sprite = this.game.resources.sprite_instances[this.sprite_id];
      sprite.render({ x: this.x, y: this.y, size: this.size, angle: this.angle });
      this.check_if_should_delete_object();
    }
    check_if_should_delete_object() {
      let sprite = this.game.resources.sprite_instances[this.sprite_id];
      if (!sprite) {
        this.destroy_object();
      }
    }
  }

  class SNHittableGameObject extends SNMovableGameObject {
    constructor(params) {
      super(params);
      this.hitbox_radius = params.hitbox_radius;
      this.score_worth = 0;
    }
    real_distance_to_hittable(object) {
      return this.distance_to(object) - this.hitbox_radius - object.hitbox_radius;
    }
    collides_with(object) {
      return this.real_distance_to_hittable(object) < 0;
    }
  }

  class SNDestroyableGameObject extends SNHittableGameObject {
    constructor(params) {
      super(params);
      this.health = params.health;
      this.kill_sprite_name = params.kill_sprite_name;
      this.kill_sound_name = params.kill_sound_name;
      this.kill_sound_volume = params.kill_sound_volume;
    }
    damage(dmg) {
      this.health -= dmg;

      if (this instanceof SNPlayer) {
        this.stats.damage_taken += dmg;
      }

      if (this.health <= 0) {
        this.kill();
      }
    }
    kill() {
      new SNSpriteObject({ game: this.game, sprite_name: this.kill_sprite_name, x: this.x, y: this.y, velocity: this.velocity, direction: this.direction, size: this.size });
      this.game.resources.sounds[this.kill_sound_name].play(this.kill_sound_volume);
      this.destroy_object();
    }
    get alive() {
      return this.health > 0;
    }
  }

  class SNAsteroid extends SNDestroyableGameObject {
    constructor(params) {
      super(params);
      this.x = Math.random() * this.game.settings.width;
      this.angle = 0;
      this.kill_sprite_name = "smoke_explosion2";
      this.kill_sound_volume = 100;
      this.kill_sound_name = "explosion2";
      this.size = params.size;
      this.hitbox_radius = this.size * 0.45; // works well with current images
      this.y = -this.size / 2;
      this.breaks_into = params.breaks_into;
      this.image = this.game.resources.images["asteroid" + Math.round(Math.random()*2+1)];
      this.rotate_per_second = 48 * this.velocity / this.size; // decorative
      if (!this.direction) this.direction = Math.random()*30-15;
    }
    update_position(tick) {
      super.update_position(tick);
      this.angle += this.rotate_per_second * tick;
    }
    render() {
      this.image.render({ x: this.x, y: this.y, size_x: this.size, size_y: this.size, angle: this.angle})
    }
    kill() {
      super.kill();
      for (let i=0; i<this.breaks_into; i++) {
        new SNAsteroidPiece({ game: this.game, x: this.x, y: this.y, velocity: this.velocity + Math.random()*100, direction: this.direction + Math.random()*120-60, health: 1 });
      }
      new SNGoldNugget({ game: this.game, x: this.x, y: this.y, velocity: this.velocity, direction: this.direction + Math.random()*30-15 });
    }
  }

  class SNPickup extends SNHittableGameObject {
    constructor(params) {
      super(params);
      this.angle = 180;
      this.image = this.game.resources.images[params.image];
      this.size = params.size;
    }
    render() {
      this.image.render({ x: this.x, y: this.y, size_x: this.size, size_y: this.size, angle: this.angle})
    }
  }

  class SNSpaceShip extends SNDestroyableGameObject {
    constructor(params) {
      super(params);
      this.size = params.size;
      this.image = this.game.resources.images[params.image];
      this.max_velocity = params.max_velocity;
    }
    render() {
      this.image.render({ x: this.x, y: this.y, size_x: this.size, size_y: this.size, angle: this.direction})
      if (this.weapon) {
        this.weapon.render();
      }
    }
  }

  class SNEnemyShip extends SNSpaceShip {
    constructor(params) {
      super(params);
      this.x = Math.random() * this.game.settings.width;
      this.y = -this.size / 2;
      this.hitbox_radius = this.size * 0.47;
      this.kill_sprite_name = "explosion1";
      this.kill_sound_name = "explosion1";

      this.change_ai("standard");
    }
    change_ai(ai_type = "standard") {
      switch (ai_type) {
        case "standard": this.ai = new SNEnemyAIStandard({ owner: this }); break;
        case "aiming":   this.ai = new SNEnemyAIAiming({ owner: this }); break;
        case "sniper":   this.ai = new SNEnemyAISniper({ owner: this }); break;
        case "chasing":  this.ai = new SNEnemyAIChasing({ owner: this }); break;
      }
    }
  }

  class SNEnemyAI {
    constructor(params) {
      this.owner = params.owner;
    }
    behave() { }
  }

  class SNEnemyAIStandard extends SNEnemyAI {
    constructor(params) {
      super(params);
    }
    behave() {
      let player = this.owner.game.player;
      this.owner.direction = 0;
      this.owner.set_velocity(this.owner.max_velocity);
      if (player.alive) {
        this.owner.weapon.fire();
      }
    }
  }

  class SNEnemyAIAiming extends SNEnemyAI {
    constructor(params) {
      super(params);
    }
    behave() {
      let player = this.owner.game.player;
      this.owner.direction = 0;
      this.owner.set_velocity(this.owner.max_velocity);
      if (player.alive) {
        this.owner.weapon.aim(player);
        this.owner.weapon.fire();
      }
    }
  }

  class SNEnemyAISniper extends SNEnemyAI {
    constructor(params) {
      super(params);
    }
    behave() {
      let player = this.owner.game.player;
      this.owner.direction = 0;
      this.owner.set_velocity(this.owner.max_velocity);
      if (player.alive) {
        if (this.owner.y >= 10 + this.owner.size / 2) {
          this.owner.set_velocity(0);
        }
        this.owner.weapon.aim(player);
        this.owner.weapon.fire();
      }
    }
  }

  class SNEnemyAIChasing extends SNEnemyAI {
    constructor(params) {
      super(params);
    }
    behave() {
      let player = this.owner.game.player;
      this.owner.set_velocity(this.owner.max_velocity);
      if (player.alive) {
        if (this.owner.distance_to(player) <= 200) {
          this.owner.set_velocity(0);
        }
        this.owner.direction = this.owner.angle_to(player);
        this.owner.weapon.fire();
      }
    }
  }

  class SNWeapon {
    constructor(params) {
      this.owner = params.owner;
      this.set_weapon_specifics();
      this.last_fired_at = 0;
      this.aiming_angle = 0;
      this.max_aiming_angle = 60;
      this.level = 1;
    }
    get is_min_level() {
      return this.level == 1;
    }
    get is_max_level() {
      return this.level == 5;
    }
    set_weapon_specifics() { // implement in specific class
      // example: this.firing_points = { 1: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -1, angle: 0 })], 2: [], ..., 5: []};, where each key is the weapon level
      this.firing_points = {};
      this.firing_sound = this.owner.game.resources.sounds.laser1;
      this.firing_sound_volume = 7;
      this.cooldown = 0.1;
      this.enemy_cooldown = 1;
      this.gun_image = null;
      this.gun_size = 0;
    }
    default_bullet_params() {
      return { game: this.owner.game, weapon: this };
    }
    bullet_params() { // implement in specific class
      // example: return { image: "laser1", hit_sprite_name: "laser1_hit", velocity: 400, damage: 1, size: 30, hitbox_radius: 3 };
      return {};
    }
    final_bullet_params() {
      return Object.assign(this.default_bullet_params(), this.bullet_params());
    }
    aim(object) {
      let absolute_angle = this.owner.angle_to(object);
      this.aiming_angle = absolute_angle - this.owner.direction;
    }
    fire() {
      if (this.ready_to_fire() && Math.abs(this.aiming_angle) <= this.max_aiming_angle) {
        this.firing_points[this.level].forEach((firing_point) => {
          firing_point.fire();
        });
        this.firing_sound.play(this.firing_sound_volume);
        this.last_fired_at = this.owner.game.logic.seconds_passed;
      }
    }
    ready_to_fire() {
      let now = this.owner.game.logic.seconds_passed;
      let cooldown = this.cooldown;
      if (this.owner instanceof SNEnemyShip) {
        cooldown = this.enemy_cooldown;
      }
      return (now - this.last_fired_at >= cooldown);
    }
    render() {
      this.firing_points[this.level].forEach((firing_point) => {
        let absolute = firing_point.absolute_position_and_direction;
        this.gun_image.render({ x: absolute.x, y: absolute.y, angle: absolute.direction, size_x: this.gun_size, size_y: this.gun_size })
      })
    }
  }

  class SNWeaponFiringPoint {
    constructor(params) {
      this.weapon = params.weapon;
      this.rel_x = params.rel_x;
      this.rel_y = params.rel_y;
      this.rel_angle = params.angle; // added to the owner, CCW
    }
    get absolute_position_and_direction() {
      // Get the relative position where to shoot from, rotated around the owners center by his own direction
      let fp_radius = Math.sqrt(this.rel_x * this.rel_x + this.rel_y * this.rel_y);
      let fp_radius_angle = Math.sign(this.rel_x) * Math.acos(Math.abs(this.rel_y) / fp_radius);
      let orientation = this.weapon.owner.direction * 2 * Math.PI / 360;
      let rel_x = fp_radius * Math.sin(orientation + fp_radius_angle);
      let rel_y = fp_radius * Math.cos(orientation + fp_radius_angle);

      let x = this.weapon.owner.x + rel_x;
      let y = this.weapon.owner.y + rel_y;
      let direction = this.rel_angle + this.weapon.owner.direction + this.weapon.aiming_angle;

      return { x: x, y: y, direction: direction };
    }
    fire() {
      let params = this.weapon.final_bullet_params();
      let absolute = this.absolute_position_and_direction;
      params.x = absolute.x;
      params.y = absolute.y;
      params.direction = absolute.direction;
      new SNBullet(params);
      if (this.weapon.owner instanceof SNPlayer) {
        this.weapon.owner.stats.bullets_fired++;
      }
    }
  }

  class SNBullet extends SNHittableGameObject {
    constructor(params) {
      super(params);
      this.weapon = params.weapon;
      this.image = this.game.resources.images[params.image];
      this.hit_sprite_name = params.hit_sprite_name;
      this.hit_sound = params.hit_sound;
      this.hit_sound_volume = params.hit_sound_volume;
      this.size = params.size;

      this.damage = params.damage;
    }
    render() {
      this.image.render({ x: this.x, y: this.y, size_x: this.size * (this.weapon.owner.has_double_damage ? 2 : 1), size_y: this.size, angle: this.direction})
    }
    hit(object) {
      let damage = this.damage;
      if (this.weapon.owner.has_double_damage) {
        damage *= 2;
      }
      object.damage(damage);
      this.destroy_object();
      new SNSpriteObject({ game: this.game, sprite_name: this.hit_sprite_name, x: this.x, y: this.y, velocity: object.velocity, direction: object.direction, size: this.size });
      this.game.resources.sounds[this.hit_sound].play(this.hit_sound_volume);

      if (this.weapon.owner instanceof SNPlayer) {
        this.weapon.owner.stats.bullets_hit++;
        this.weapon.owner.stats.damage_dealt += damage;
      } else if (object instanceof SNPlayer) {
        object.stats.bullets_taken++;
        object.stats.remove_bullet_closely_dodged(this);
      }
    }
  }

  class SNBackground {
    constructor(params) {
      this.game = params.game;
      this.speed = 200;
      this.stars = {};
      this.star_density = 0.5;
      this.change({ image: "background3" });
      this.y = -this.image.image.height;
    }
    render() {
      let y = this.y;

      while (y <= this.game.settings.height) {
        let x = 0;
        while (x <= this.game.settings.width) {
          this.image.render({ x: x, y: y });
          x += this.image.image.width;
        }
        y += this.image.image.height;
      }

      if (this.game.settings.draw_background_stars) {
        for (let id in this.stars) {
          let star = this.stars[id];
          star.render();
        }
      }
    }
    update(tick) {
      this.y += this.speed * tick;
      if (this.y >= 0) {
        this.y = -this.image.image.height;
      }

      if (this.game.settings.draw_background_stars) {
        for (let id in this.stars) {
          let star = this.stars[id];
          star.update_position(tick);
        }
        if (Math.random() < this.star_density) {
          new SNBackgroundStar({ background: this });
        }
      }
    }
    change(params) {
      this.image = this.game.resources.images[params.image];
    }
  }

  class SNBackgroundStar {
    constructor(params) {
      this.background = params.background;

      this.x = Math.round((Math.random() * this.background.game.settings.width) + 1);
      this.y = -5;
      this.speed = Math.random() * 200 + 100;
      this.visibility = Math.random()*0.6 + 0.2;
      this.size = Math.random() + 1;
      this.color = "rgb(" + Math.round((Math.random() * 100) + 155) + ", " + Math.round((Math.random() * 100) + 155) + ", " + Math.round((Math.random() * 100) + 155) + ")";

      this.id = Date.now();
      while(this.background.stars[this.id]) this.id++;
      this.background.stars[this.id] = this;
    }
    render() {
      let ctx = this.background.game.ctx;
      ctx.fillStyle = this.color;
      ctx.globalAlpha = this.visibility;
      ctx.fillRect(this.x, this.y, this.size, this.size);
      ctx.globalAlpha = 1.0;
    }
    update_position(tick) {
      this.y += this.speed * tick;
      if (this.y >= this.background.game.settings.height) {
        this.destroy_object();
      }
    }
    destroy_object() {
      delete this.background.stars[this.id];
    }
  }

////////////////////////////////////////////////////
///////////////////////////// SPECIFIC GAME OBJECTS
////////////////////////////////////////////////////

/////// PLAYER

  class SNPlayer extends SNSpaceShip {
    constructor(params) {
      super(params);
      this.destroy_object(); // this only removes the player from the object_list
      this.direction = 180; // always faces upwards
      this.weapon = new SNWeaponGatlingMk1({ owner: this })
      this.health = 5;
      this.kill_sprite_name = "explosion1";
      this.kill_sound_name = "explosion1";

      this.has_double_damage = false;
      this.last_double_damage = null;
      this.double_damage_duration = 0;

      this.stats = new SNPlayerStats({ owner: this });
    }
    update_position(tick) {
      let margin = 5;
      if (this.game.keyboard.move_up()) {
        if (this.y > 0 + this.size / 2 + margin) {
          this.y -= this.max_velocity * tick * 1.2;
        }
      }
      if (this.game.keyboard.move_down()) {
        if (this.y < this.game.settings.height - this.size / 2 - margin - 15) {
          this.y += this.max_velocity * tick * 0.8;
        }
      }
      if (this.game.keyboard.move_left()) {
        if (this.x > 0 + this.size / 2 + margin) {
          this.x -= this.max_velocity * tick;
        }
      }
      if (this.game.keyboard.move_right()) {
        if (this.x < this.game.settings.width - this.size / 2 - margin) {
          this.x += this.max_velocity * tick;
        }
      }
      if (this.game.keyboard.fire()) {
        this.weapon.fire();
      }
    }
    update_effects(tick) {
      if (this.has_double_damage && this.last_double_damage + this.double_damage_duration < this.game.logic.seconds_passed) {
        this.has_double_damage = false;
      }
      if (this.health > 0) {
        this.stats.seconds_passed += tick;
        if (this.has_double_damage) {
          this.stats.total_time_in_double_damage += tick;
        }
      }
    }
    kill() {
      super.kill();
      this.game.logic.player_dies();
    }
  }

/////// WEAPONS

  class SNWeaponGatlingMk1 extends SNWeapon {
    constructor(params) {
      super(params);
    }
    get messages() {
      return {
        color: "#ffffcc",
        on_pickup: "COOL MACHINEGUN!",
        on_levelup: "MORE GUNS!",
        on_already_max_level: "Already max!"
      }
    }
    set_weapon_specifics() {
      this.firing_points = {
        1: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 })],
        2: [new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 0 })],
        3: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 5 })],
        4: [new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -25, rel_y: -3, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 25, rel_y: -3, angle: 5 })],
        5: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: -2 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 2 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -25, rel_y: -3, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 25, rel_y: -3, angle: 5 })],
      };
      this.cooldown = 0.15;
      this.enemy_cooldown = 1;
      this.pickup_sound = this.owner.game.resources.sounds.machinegun_pickup;
      this.firing_sound = this.owner.game.resources.sounds.machinegun_shot1;
      this.firing_sound_volume = 7;
      this.gun_image = this.owner.game.resources.images.gun1;
      this.gun_size = 20;
    }
    bullet_params() {
      return { image: "laser4", hit_sprite_name: "laser4_hit_long", velocity: 730, damage: 1, size: 30, hitbox_radius: 3, hit_sound: "bullet_hit", hit_sound_volume: 70 };
    }
  }

  class SNWeaponGatlingMk2 extends SNWeapon {
    constructor(params) {
      super(params);
    }
    get messages() {
      return {
        color: "#ffcccc",
        on_pickup: "GATLING MK2!",
        on_levelup: "MORE LASERS!",
        on_already_max_level: "Already max!"
      }
    }
    set_weapon_specifics() {
      this.firing_points = {
        1: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 })],
        2: [new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 0 })],
        3: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 5 })],
        4: [new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -25, rel_y: -3, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 25, rel_y: -3, angle: 5 })],
        5: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: -2 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 2 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -25, rel_y: -3, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 25, rel_y: -3, angle: 5 })],
      };
      this.cooldown = 0.115;
      this.enemy_cooldown = 1;
      this.pickup_sound = this.owner.game.resources.sounds.laser_pickup;
      this.firing_sound = this.owner.game.resources.sounds.laser_shot1;
      this.firing_sound_volume = 7;
      this.gun_image = this.owner.game.resources.images.gun2;
      this.gun_size = 20;
    }
    bullet_params() {
      return { image: "laser1", hit_sprite_name: "laser1_hit_long", velocity: 550, damage: 1, size: 30, hitbox_radius: 3, hit_sound: "bullet_hit", hit_sound_volume: 70 };
    }
  }

  class SNWeaponPlasmaGun extends SNWeapon {
    constructor(params) {
      super(params);
    }
    get messages() {
      return {
        color: "#ccccff",
        on_pickup: "BIG GUNS!",
        on_levelup: "F$#& YES!",
        on_already_max_level: "Already max!"
      }
    }
    set_weapon_specifics() {
      this.firing_points = {
        1: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 })],
        2: [new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 0 })],
        3: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 5 })],
        4: [new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -25, rel_y: -3, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 25, rel_y: -3, angle: 5 })],
        5: [new SNWeaponFiringPoint({ weapon: this, rel_x: 0, rel_y: -25, angle: 0 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -17, rel_y: -8, angle: -2 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 17, rel_y: -8, angle: 2 }), new SNWeaponFiringPoint({ weapon: this, rel_x: -25, rel_y: -3, angle: -5 }), new SNWeaponFiringPoint({ weapon: this, rel_x: 25, rel_y: -3, angle: 5 })],
      };
      this.cooldown = 0.3;
      this.enemy_cooldown = 2;
      this.pickup_sound = this.owner.game.resources.sounds.plasma_pickup;
      this.pickup_sound_volume = 100;
      this.firing_sound = this.owner.game.resources.sounds.plasma_shot1;
      this.firing_sound_volume = 100;
      this.gun_image = this.owner.game.resources.images.gun5;
      this.gun_size = 20;
    }
    bullet_params() {
      return { image: "laser2", hit_sprite_name: "laser2_hit_long", velocity: 700, damage: 3, size: 30, hitbox_radius: 4, hit_sound: "bullet_hit", hit_sound_volume: 70 };
    }
  }

/////// PICKUPS

  class SNGoldNugget extends SNPickup {
    constructor(params) {
      params.image = "gold_nugget";
      super(params);

      this.size = 20;
      this.hitbox_radius = 10;

      this.rotate_per_second = 100;
    }
    update_position(tick) {
      super.update_position(tick);
      this.angle += this.rotate_per_second * tick;
    }
  }

  class SNPickupHealth extends SNPickup {
    constructor(params) {
      params.image = "pickup_health";
      super(params);

      this.x = Math.round((Math.random() * (this.game.settings.width - 40)) + 20);
      this.y = -17;
      this.direction = 0;
      this.velocity = 90;

      this.size = 33;
      this.hitbox_radius = 17;
    }
  }

  class SNPickupDoubleDamage extends SNPickup {
    constructor(params) {
      params.image = "pickup_powerup";
      super(params);

      this.x = Math.round((Math.random() * (this.game.settings.width - 40)) + 20);
      this.y = -17;
      this.direction = 0;
      this.velocity = 120;

      this.size = 33;
      this.hitbox_radius = 17;
    }
  }

  class SNPickupWeapon extends SNPickup {
    constructor(params) {
      super(params);

      this.x = Math.round((Math.random() * (this.game.settings.width - 40)) + 20);
      this.y = -17;
      this.direction = 0;
      this.velocity = 120;

      this.size = 40;
      this.hitbox_radius = 17;

      this.weapon_class = params.weapon_class;
    }
  }

/////// OTHER

  class SNAsteroidPiece extends SNDestroyableGameObject {
    constructor(params) {
      super(params);
      this.angle = 0;
      this.rotate_per_second = 100;
      this.kill_sprite_name = "smoke_explosion2";
      this.kill_sound_name = "explosion2";
      this.kill_sound_volume = 10;
      this.size = 20;
      this.hitbox_radius = 10;
      this.image = this.game.resources.images["asteroid_piece" + Math.round(Math.random()+1)];
    }
    update_position(tick) {
      super.update_position(tick);
      this.angle += this.rotate_per_second * tick;
    }
    render() {
      this.image.render({ x: this.x, y: this.y, size_x: this.size, size_y: this.size, angle: this.angle})
    }
  }





//////////////////////////////////////////////
///////////////////////////// RESOURCE SYSTEM
//////////////////////////////////////////////

  class SNResource {
    constructor(params) {
      this.manager = params.manager;
      this.manager.total++;
      this.name = params.name;
    }
    ready() {
      this.manager.ready++;
    }
  }

  class SNImageResource extends SNResource {
    constructor(params) {
      super(params)
      this.image = new Image();
      this.image.src = params.src;

      this.image.onload = () => {
        this.ready();
      }

      this.manager.images[this.name] = this;
    }
    render(params) {
      if (typeof params.size_x == "undefined") params.size_x = this.image.width;
      if (typeof params.size_y == "undefined") params.size_y = this.image.height;
      this.manager.game.ctx.drawImage(this.image, params.x, params.y, params.size_x, params.size_y);
    }
  }

  class SNCentralizedImageResource extends SNImageResource {
    constructor(params) {
      super(params);
      this.cX = params.cX; // in pixels, center relative to the image 0, 0
      this.cY = params.cY;
    }
    render(params) {
      if (typeof params.size_x == "undefined") params.size_x = this.image.width;
      if (typeof params.size_y == "undefined") params.size_y = this.image.height;
      if (typeof params.angle == "undefined") params.angle = 180;

      let offset_x = this.cX * params.size_x / this.image.width;
      let offset_y = this.cY * params.size_y / this.image.height;

      let ctx = this.manager.game.ctx;
      ctx.save();
      ctx.translate(params.x, params.y);
      ctx.rotate((180 - params.angle) * (2* Math.PI) / 360);
      ctx.translate(-params.x, -params.y);
      ctx.drawImage(this.image, params.x - offset_x, params.y - offset_y, params.size_x, params.size_y);
      ctx.restore();
    }
  }

  class SNSpriteResource extends SNCentralizedImageResource {
    constructor(params) {
      super(params);
      this.frame_size = params.frame_size;
      this.max_frames = params.max_frames;
      this.looping = params.looping || false;
    }
    new_instance(params) {
      let instance = new SNSpriteInstance({ sprite: this });
      return this.manager.sprite_instances[instance.id]; // return the reference from the list
    }
  }

  class SNSpriteInstance {
    constructor(params) {
      this.sprite = params.sprite;
      this.current_frame = 0;
      this.sprite_x = 0;
      this.sprite_y = 0;

      this.id = Date.now();
      while(this.sprite.manager.sprite_instances[this.id]) this.id++;
      this.sprite.manager.sprite_instances[this.id] = this;
    }
    render(params) {
      if (typeof params.size == "undefined") params.size = this.sprite.frame_size;
      if (typeof params.angle == "undefined") params.angle = 180;

      let offset_x = this.sprite.cX * params.size / this.sprite.frame_size;
      let offset_y = this.sprite.cY * params.size / this.sprite.frame_size;

      let ctx = this.sprite.manager.game.ctx;
      ctx.save();
      ctx.translate(params.x, params.y);
      ctx.rotate((180 - params.angle) * (2* Math.PI) / 360);
      ctx.translate(-params.x, -params.y);
      ctx.drawImage(this.sprite.image, this.sprite_x, this.sprite_y, this.sprite.frame_size, this.sprite.frame_size, params.x - offset_x, params.y - offset_y, params.size, params.size);
      ctx.restore();

      this.prepare_next_frame();
    }
    prepare_next_frame() {
      this.current_frame++;
      if (this.current_frame >= this.sprite.max_frames && !this.sprite.looping) {
        this.destroy_instance();
      }
      this.sprite_x += this.sprite.frame_size;
      if (this.sprite_x >= this.sprite.image.width) {
        this.sprite_x = 0;
        this.sprite_y += this.sprite.frame_size;
      }
      if (this.sprite_y >= this.sprite.image.height) {
        this.current_frame = 0;
        this.sprite_x = 0;
        this.sprite_y = 0;
      }
    }
    destroy_instance() {
      delete this.sprite.manager.sprite_instances[this.id];
    }
  }

  class SNSoundResource extends SNResource {
    constructor(params) {
      super(params);
      this.manager.sounds[this.name] = this;

      let request = new XMLHttpRequest();
      request.open('GET', params.src, true);
      request.responseType = 'arraybuffer';
      request.onload = () => {
        this.manager.audio_ctx.decodeAudioData(request.response, (buffer) => {
          this.sound_buffer = buffer;
          this.ready();
        });
      }
      request.send();
    }
    play(volume = 100) {
      let audio_ctx = this.manager.audio_ctx;
      let gain_node = audio_ctx.createGain();
      let source = audio_ctx.createBufferSource();
      source.buffer = this.sound_buffer;
      source.connect(gain_node);
      gain_node.connect(audio_ctx.destination);

      gain_node.gain.setValueAtTime(volume / 100, audio_ctx.currentTime);
      source.start(0);
    }
  }

  class SNMusicResource extends SNResource {
    constructor(params) {
      super(params);
      this.manager.music[this.name] = this;
      this.audio = new Audio(params.src);
      this.audio.volume = params.volume || 1;
      this.ready();
    }
    play() {
      this.audio.addEventListener("ended", () => { this.replay(); });
      this.audio.play();
    }
    replay() {
      this.audio.currentTime = 0;
      this.audio.play();
    }
    stop() {
      this.pause();
      this.audio.removeEventListener("ended", () => { this.replay(); });
      this.audio.currentTime = 0;
    }
    pause() {
      this.audio.pause();
    }
  }

  class SNFontResource extends SNResource {
    constructor(params) {
      super(params);
      this.font_family = params.font_family;
      this.manager.fonts[this.name] = this;
      this.ready();

      let style_element = document.createElement("style");
      style_element.innerHTML = "@font-face { font-family: '" + this.font_family + "'; src: url('" + params.src + "'); }";
      document.getElementById("sn_game_wrapper").appendChild(style_element);
    }
  }

  class SNResourceManager {
    constructor(params) {
      this.game = params.game;
      this.root_path = params.root_path;

      this.total = 0;
      this.ready = 0;
      this.images = {};
      this.sounds = {};
      this.music = {};
      this.sprite_instances = {};
      this.fonts = {};

      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      this.audio_ctx = new AudioContext();
    }
    load_resources() {
      // images
      new SNCentralizedImageResource({ manager: this, name: "player", src: this.root_path + "assets/images/ships/player.png", cX: 56, cY: 70 });
      new SNImageResource({ manager: this, name: "background1", src: this.root_path + "assets/images/backgrounds/black.png" });
      new SNImageResource({ manager: this, name: "background2", src: this.root_path + "assets/images/backgrounds/blue.png" });
      new SNImageResource({ manager: this, name: "background3", src: this.root_path + "assets/images/backgrounds/dark_purple.png" });
      new SNImageResource({ manager: this, name: "background4", src: this.root_path + "assets/images/backgrounds/purple.png" });
      new SNCentralizedImageResource({ manager: this, name: "game_logo", src: this.root_path + "assets/images/logo/game_logo_normal.png", cX: 250, cY: 328 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlack1", src: this.root_path + "assets/images/ships/enemyBlack1.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlack2", src: this.root_path + "assets/images/ships/enemyBlack2.png", cX: 52, cY: 58 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlack3", src: this.root_path + "assets/images/ships/enemyBlack3.png", cX: 51, cY: 51 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlack4", src: this.root_path + "assets/images/ships/enemyBlack4.png", cX: 42, cY: 42 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlack5", src: this.root_path + "assets/images/ships/enemyBlack5.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlue1", src: this.root_path + "assets/images/ships/enemyBlue1.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlue2", src: this.root_path + "assets/images/ships/enemyBlue2.png", cX: 52, cY: 58 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlue3", src: this.root_path + "assets/images/ships/enemyBlue3.png", cX: 51, cY: 51 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlue4", src: this.root_path + "assets/images/ships/enemyBlue4.png", cX: 42, cY: 42 });
      new SNCentralizedImageResource({ manager: this, name: "shipBlue5", src: this.root_path + "assets/images/ships/enemyBlue5.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipGreen1", src: this.root_path + "assets/images/ships/enemyGreen1.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipGreen2", src: this.root_path + "assets/images/ships/enemyGreen2.png", cX: 52, cY: 58 });
      new SNCentralizedImageResource({ manager: this, name: "shipGreen3", src: this.root_path + "assets/images/ships/enemyGreen3.png", cX: 51, cY: 51 });
      new SNCentralizedImageResource({ manager: this, name: "shipGreen4", src: this.root_path + "assets/images/ships/enemyGreen4.png", cX: 42, cY: 42 });
      new SNCentralizedImageResource({ manager: this, name: "shipGreen5", src: this.root_path + "assets/images/ships/enemyGreen5.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipRed1", src: this.root_path + "assets/images/ships/enemyRed1.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "shipRed2", src: this.root_path + "assets/images/ships/enemyRed2.png", cX: 52, cY: 58 });
      new SNCentralizedImageResource({ manager: this, name: "shipRed3", src: this.root_path + "assets/images/ships/enemyRed3.png", cX: 51, cY: 51 });
      new SNCentralizedImageResource({ manager: this, name: "shipRed4", src: this.root_path + "assets/images/ships/enemyRed4.png", cX: 42, cY: 42 });
      new SNCentralizedImageResource({ manager: this, name: "shipRed5", src: this.root_path + "assets/images/ships/enemyRed5.png", cX: 47, cY: 47 });
      new SNCentralizedImageResource({ manager: this, name: "laser1", src: this.root_path + "assets/images/bullets/laser1.png", cX: 18, cY: 6 });
      new SNCentralizedImageResource({ manager: this, name: "laser2", src: this.root_path + "assets/images/bullets/laser2.png", cX: 18, cY: 6 });
      new SNCentralizedImageResource({ manager: this, name: "laser3", src: this.root_path + "assets/images/bullets/laser3.png", cX: 24, cY: 24 });
      new SNCentralizedImageResource({ manager: this, name: "laser4", src: this.root_path + "assets/images/bullets/laser4.png", cX: 18, cY: 6 });
      new SNCentralizedImageResource({ manager: this, name: "asteroid1", src: this.root_path + "assets/images/asteroids/asteroid1.png", cX: 50, cY: 50 });
      new SNCentralizedImageResource({ manager: this, name: "asteroid2", src: this.root_path + "assets/images/asteroids/asteroid2.png", cX: 45, cY: 45 });
      new SNCentralizedImageResource({ manager: this, name: "asteroid3", src: this.root_path + "assets/images/asteroids/asteroid3.png", cX: 44, cY: 44 });
      new SNCentralizedImageResource({ manager: this, name: "asteroid_piece1", src: this.root_path + "assets/images/asteroids/asteroid_piece1.png", cX: 14, cY: 14 });
      new SNCentralizedImageResource({ manager: this, name: "asteroid_piece2", src: this.root_path + "assets/images/asteroids/asteroid_piece2.png", cX: 14, cY: 14 });
      new SNCentralizedImageResource({ manager: this, name: "gold_nugget", src: this.root_path + "assets/images/pickups/gold_nugget1.png", cX: 14, cY: 14 });
      new SNCentralizedImageResource({ manager: this, name: "pickup_health", src: this.root_path + "assets/images/pickups/pickup_health.png", cX: 16, cY: 16 });
      new SNCentralizedImageResource({ manager: this, name: "pickup_powerup", src: this.root_path + "assets/images/pickups/pickup_powerup.png", cX: 16, cY: 16 });
      new SNCentralizedImageResource({ manager: this, name: "crate1", src: this.root_path + "assets/images/pickups/crate1.png", cX: 36, cY: 36 });
      new SNCentralizedImageResource({ manager: this, name: "crate2", src: this.root_path + "assets/images/pickups/crate2.png", cX: 36, cY: 36 });
      new SNCentralizedImageResource({ manager: this, name: "crate3", src: this.root_path + "assets/images/pickups/crate3.png", cX: 36, cY: 36 });
      new SNCentralizedImageResource({ manager: this, name: "gun1", src: this.root_path + "assets/images/weapons/gun1.png", cX: 18, cY: 20 });
      new SNCentralizedImageResource({ manager: this, name: "gun2", src: this.root_path + "assets/images/weapons/gun2.png", cX: 20, cY: 21 });
      new SNCentralizedImageResource({ manager: this, name: "gun3", src: this.root_path + "assets/images/weapons/gun3.png", cX: 18, cY: 26 });
      new SNCentralizedImageResource({ manager: this, name: "gun4", src: this.root_path + "assets/images/weapons/gun4.png", cX: 19, cY: 28 });
      new SNCentralizedImageResource({ manager: this, name: "gun5", src: this.root_path + "assets/images/weapons/gun5.png", cX: 16, cY: 23 });
      new SNCentralizedImageResource({ manager: this, name: "player_life", src: this.root_path + "assets/images/icons/player_life.png", cX: 18, cY: 18 });

      // sprites
      new SNSpriteResource({ manager: this, name: "explosion1", src: this.root_path + "assets/images/sprites/explosion1.png", cX: 50, cY: 50, frame_size: 100, max_frames: 81 });
      new SNSpriteResource({ manager: this, name: "smoke_explosion1", src: this.root_path + "assets/images/sprites/smoke_explosion1.png", cX: 64, cY: 64, frame_size: 128, max_frames: 64 });
      new SNSpriteResource({ manager: this, name: "smoke_explosion2", src: this.root_path + "assets/images/sprites/smoke_explosion2.png", cX: 50, cY: 50, frame_size: 100, max_frames: 81 });
      new SNSpriteResource({ manager: this, name: "electricExplosion1", src: this.root_path + "assets/images/sprites/electricExplosion1.png", cX: 32, cY: 32, frame_size: 64, max_frames: 10 });
      new SNSpriteResource({ manager: this, name: "electricExplosion2", src: this.root_path + "assets/images/sprites/electricExplosion2.png", cX: 32, cY: 32, frame_size: 64, max_frames: 10 });
      new SNSpriteResource({ manager: this, name: "laser1_hit", src: this.root_path + "assets/images/sprites/laser1_hit.png", cX: 24, cY: 24, frame_size: 48, max_frames: 4 });
      new SNSpriteResource({ manager: this, name: "laser1_hit_long", src: this.root_path + "assets/images/sprites/laser1_hit_long.png", cX: 24, cY: 24, frame_size: 48, max_frames: 6 });
      new SNSpriteResource({ manager: this, name: "laser2_hit_long", src: this.root_path + "assets/images/sprites/laser2_hit_long.png", cX: 24, cY: 24, frame_size: 48, max_frames: 6 });
      new SNSpriteResource({ manager: this, name: "laser4_hit_long", src: this.root_path + "assets/images/sprites/laser4_hit_long.png", cX: 24, cY: 24, frame_size: 48, max_frames: 6 });
      // sounds
      new SNSoundResource({ manager: this, name: "laser_pickup", src: this.root_path + "assets/sounds/laser_pickup.ogg" });
      new SNSoundResource({ manager: this, name: "laser_shot1", src: this.root_path + "assets/sounds/laser_shot1.ogg" });
      new SNSoundResource({ manager: this, name: "bullet_hit", src: this.root_path + "assets/sounds/bullet_hit.ogg" });
      new SNSoundResource({ manager: this, name: "machinegun_pickup", src: this.root_path + "assets/sounds/machinegun_pickup.ogg" });
      new SNSoundResource({ manager: this, name: "machinegun_shot1", src: this.root_path + "assets/sounds/machinegun_shot1.ogg" });
      new SNSoundResource({ manager: this, name: "machinegun_shot2", src: this.root_path + "assets/sounds/machinegun_shot2.ogg" });
      new SNSoundResource({ manager: this, name: "plasma_pickup", src: this.root_path + "assets/sounds/plasma_pickup.ogg" });
      new SNSoundResource({ manager: this, name: "plasma_shot1", src: this.root_path + "assets/sounds/plasma_shot1.ogg" });

      new SNSoundResource({ manager: this, name: "explosion1", src: this.root_path + "assets/sounds/explosion1.ogg" });
      new SNSoundResource({ manager: this, name: "explosion2", src: this.root_path + "assets/sounds/explosion2.ogg" });
      new SNSoundResource({ manager: this, name: "pickup1", src: this.root_path + "assets/sounds/pickup1.ogg" });
      new SNSoundResource({ manager: this, name: "pickup2", src: this.root_path + "assets/sounds/pickup2.ogg" });
      new SNSoundResource({ manager: this, name: "pickup3", src: this.root_path + "assets/sounds/pickup3.ogg" });
      new SNSoundResource({ manager: this, name: "double_damage_7sec", src: this.root_path + "assets/sounds/double_damage_7sec.ogg" });

      // music
      new SNMusicResource({ manager: this, name: "music1", src: this.root_path + "assets/sounds/music1.ogg" });
      new SNMusicResource({ manager: this, name: "music2", src: this.root_path + "assets/sounds/music2.ogg", volume: 0.15 });
      // fonts
      new SNFontResource({ manager: this, name: "terran", font_family: "Terran", src: this.root_path + "assets/fonts/terran.ttf"});
    }
    get all_loaded() {
      return this.total == this.ready;
    }
  }
