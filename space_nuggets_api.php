<?php
  $result = new stdClass();

  $server = "localhost";
  $username = "sn_user";
  $password = "123456789";
  $db_name = "space_nuggets";

  $conn = @(new mysqli($server, $username, $password, $db_name));
  if ($conn->connect_error) {
    $result->error = "Database connection error!";
    die(json_encode($result));
  }

  if (false && !isset($_GET["action"])) {
    $result->error = "Missing param: 'action'";
    die(json_encode($result));
  }

  if ($_GET["action"] == "get_high_scores") {
    $sql = "SELECT name, score FROM game_records ORDER BY score DESC, played_at ASC LIMIT 5;";
    $rs = $conn->query($sql);
    if ($rs->num_rows > 0) {
      $result->high_scores = [];
      while($row = $rs->fetch_assoc()) {
        $result->high_scores[] = $row;
      }
    }
  } elseif ($_GET["action"] == "add_play_record") {
    if (
      !isset($_GET["name"]) ||
      !isset($_GET["score"]) ||
      !isset($_GET["bullets_hit"]) ||
      !isset($_GET["bullets_fired"]) ||
      !isset($_GET["damage_dealt"]) ||
      !isset($_GET["damage_taken"]) ||
      !isset($_GET["bullets_closely_dodged"]) ||
      !isset($_GET["enemies_killed"]) ||
      !isset($_GET["asteroids_destroyed"]) ||
      !isset($_GET["asteroid_pieces_destroyed"]) ||
      !isset($_GET["crashed_into_asteroids"]) ||
      !isset($_GET["gold_nuggets_picked_up"]) ||
      !isset($_GET["weapon_crates_picked_up"]) ||
      !isset($_GET["health_crates_picked_up"]) ||
      !isset($_GET["seconds_passed"]) ||
      !isset($_GET["total_time_in_double_damage"])) {
        $result->error = "Missing parameters for inserting";
        die(json_encode($result));
    }

    $sql = "INSERT INTO game_records (
              name, 
              score,
              bullets_hit,
              bullets_fired,
              damage_dealt,
              damage_taken,
              bullets_closely_dodged,
              enemies_killed,
              asteroids_destroyed,
              asteroid_pieces_destroyed,
              crashed_into_asteroids,
              gold_nuggets_picked_up,
              weapon_crates_picked_up,
              health_crates_picked_up,
              seconds_passed,
              total_time_in_double_damage
            )
            VALUES (
              '". $_GET["name"] ."',
              '". $_GET["score"] ."',
              '". $_GET["bullets_hit"] ."',
              '". $_GET["bullets_fired"] ."',
              '". $_GET["damage_dealt"] ."',
              '". $_GET["damage_taken"] ."',
              '". $_GET["bullets_closely_dodged"] ."',
              '". $_GET["enemies_killed"] ."',
              '". $_GET["asteroids_destroyed"] ."',
              '". $_GET["asteroid_pieces_destroyed"] ."',
              '". $_GET["crashed_into_asteroids"] ."',
              '". $_GET["gold_nuggets_picked_up"] ."',
              '". $_GET["weapon_crates_picked_up"] ."',
              '". $_GET["health_crates_picked_up"] ."',
              '". $_GET["seconds_passed"] ."',
              '". $_GET["total_time_in_double_damage"] ."'
            )";
    if (!$conn->query($sql)) {
      $result->error = "Error: " . $conn->error;
    }
  }

  $conn->close();
  echo json_encode($result);
?>
