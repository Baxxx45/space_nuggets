<head>
  <link rel="icon" type="image/png" href="favicon.png">
  <title>Space Nuggets</title>
  <style>
    html {
      background-color: #29212d;
    }
  </style>
</head>
<body>
  <div id="sn_game_wrapper" style="text-align: center;">
  </div>

  <script type="text/javascript" src="space_nuggets.js"></script>
  <script type="text/javascript">
    let game = new SpaceNuggetsGame({
      api_address: "space_nuggets_api.php",
      root_path: "",
    });
  </script>
</body>
